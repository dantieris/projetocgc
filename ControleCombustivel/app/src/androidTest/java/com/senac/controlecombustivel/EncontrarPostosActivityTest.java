package com.senac.controlecombustivel;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

/**
 * Created by Dantieris on 12/07/2015.
 */
public class EncontrarPostosActivityTest extends ActivityInstrumentationTestCase2<EncontrarPostosActivity> {

    EncontrarPostosActivity encontrarPostosActivity;

    public EncontrarPostosActivityTest() {
        super(EncontrarPostosActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        encontrarPostosActivity = getActivity();
    }

    @SmallTest
    public void testTrue() throws Exception {
        Button button = (Button) encontrarPostosActivity.findViewById(R.id.show_location);
        assertNotNull(button);
    }
}
