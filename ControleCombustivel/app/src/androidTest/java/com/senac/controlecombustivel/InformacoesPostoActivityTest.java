package com.senac.controlecombustivel;

import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;

import com.senac.controlecombustivel.model.Bandeira;
import com.senac.controlecombustivel.model.Posto;

/**
 * Created by Dantieris on 14/07/2015.
 */
public class InformacoesPostoActivityTest extends ActivityUnitTestCase<InformacoesPostoActivity> {

    private InformacoesPostoActivity informacoesPostoActivity;
    private Instrumentation instrumentation;

    public InformacoesPostoActivityTest() {
        super(InformacoesPostoActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(), InformacoesPostoActivity.class)
                .putExtra("posto", new Posto(2, "endereco", "nome", 5,5, new Bandeira(1, "Lol")));

        Bundle bundle = getInstrumentation().getBinderCounts();

        startActivity(intent, bundle, null);
    }

    public void testName() throws Exception {
        assertTrue(true);
    }
}
