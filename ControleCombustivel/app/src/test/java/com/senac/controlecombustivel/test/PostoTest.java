package com.senac.controlecombustivel.test;

import android.test.suitebuilder.annotation.SmallTest;

import com.senac.controlecombustivel.model.Posto;

import junit.framework.TestCase;

/**
 * Created by Dantieris on 12/07/2015.
 */
public class PostoTest extends TestCase {

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @SmallTest
    public void testSetGetNome() {
        Posto posto = new Posto();
        assertEquals("Testando set e get nome do posto ","Nome", "nome");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
