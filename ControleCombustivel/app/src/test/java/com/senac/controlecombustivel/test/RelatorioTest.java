package com.senac.controlecombustivel.test;

import com.senac.controlecombustivel.model.Relatorio;

import junit.framework.TestCase;

/**
 * Created by Dantieris on 12/07/2015.
 */
public class RelatorioTest extends TestCase {

    public void testValorTotal() throws Exception {
        Relatorio relatorio = new Relatorio();

        relatorio.setValorTotal(100);

        assertEquals(100, relatorio.getValorTotal(), 0.1);
    }
}
