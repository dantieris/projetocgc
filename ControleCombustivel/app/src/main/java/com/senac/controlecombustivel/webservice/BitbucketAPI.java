package com.senac.controlecombustivel.webservice;

public class BitbucketAPI {

    private static final String URL = "https://bitbucket.org/api/1.0/repositories/dantieris/projetocgc/issues/";

    private static final String USUARIO = "inappfeedback";
    private static final String SENHA = "inappfeedback";

    public static void criarIssue(BitbucketAPI.Issue issue) {
        String bodyString = issue.getBody();

        new WebServicePOST().execute(URL, bodyString.trim(), USUARIO, SENHA);
    }

    public static class Issue {
        private String estado;
        private String prioridade;
        private String titulo;
        private String conteudo;
        private String tipo;

        public Issue(String titulo, String conteudo, String tipo) {
            this.titulo = titulo;
            this.conteudo = conteudo;
            this.tipo = tipo;
            this.estado = "new";
            this.prioridade = "trivial";
        }

        public String getTipo() {
            return tipo;
        }

        public String getBody() {
            return "title=" + titulo +
                    "&content=" + conteudo +
                    "&status=" + estado +
                    "&priority=" + prioridade +
                    "&kind=" + tipo;
        }
    }
}
