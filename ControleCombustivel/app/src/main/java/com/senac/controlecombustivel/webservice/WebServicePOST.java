package com.senac.controlecombustivel.webservice;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Arrays;

public class WebServicePOST extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(final String... strings) {
        String tag = "DEBUGANDO WEB SERVICE POST";
        Log.d(tag, Arrays.toString(strings));
        URL url = null;
        String bodyString = strings[1];

        // Cria o objeto url
        try {
            url = new URL(strings[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Conecta com o url pra realizar input e output de dados.
        HttpURLConnection urlConnection = null;
        try {
            // Se o parametro strings for do tamanho 4,
            // ele contém usuário e senha para realizar autenticação do post.
            if (strings.length == 4) {
                Authenticator.setDefault(new Authenticator(){
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(strings[2],strings[3].toCharArray());
                    }});
            }

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("POST");

            urlConnection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            if (urlConnection.getDoOutput()) {
                Log.d(tag, "Do output true");
                DataOutputStream dos = new DataOutputStream(urlConnection.getOutputStream());

                dos.writeBytes(bodyString);
                dos.flush();
                dos.close();
            }

            if (urlConnection.getDoInput()) {
                InputStream inputStream = null;
                inputStream = new BufferedInputStream(urlConnection.getInputStream());
                Log.d("DEBUGANDO WS POST", lerStream(inputStream));
                inputStream.close();
            }

            urlConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
            urlConnection.disconnect();
        } finally {
            urlConnection.disconnect();
        }

        return null;
    }


    /**
     * Recebe um input stream, carrega todos dados inseridos nele em um string builder e retorna um json string.
     */
    private static String lerStream(InputStream inputStream) {
        String linha;
        BufferedReader bfr = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();

        try {
            while ((linha = bfr.readLine()) != null) {
                total.append(linha);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return total.toString();
    }
}