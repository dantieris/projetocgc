package com.senac.controlecombustivel;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.senac.controlecombustivel.webservice.BitbucketAPI;

public class ControladorFeedback {

    Context context;

    public ControladorFeedback(final Context context) {
        this.context = context;

        AlertDialog.Builder feedbackDialogo = new AlertDialog.Builder(context);

        Activity activity = (Activity) context;

        // Get the layout inflater
        LayoutInflater inflater = activity.getLayoutInflater();

        final View view = inflater.inflate(R.layout.feedback_dialog, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        feedbackDialogo.setView(view);

        feedbackDialogo.setIcon(R.drawable.ic_feedback_black_48dp);
        feedbackDialogo.setTitle("Feedback");
        feedbackDialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        feedbackDialogo.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                criarIssue(view);

                dialog.dismiss();

                Toast.makeText(context, "Obrigado pelo feedback!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        AlertDialog dialogo = feedbackDialogo.create();

        dialogo.show();

        Float densidade = context.getResources().getDisplayMetrics().density;

        Log.d("DENSIDADE", String.valueOf(densidade));

        if (densidade >= 1.5) {
            dialogo.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } else {
            dialogo.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }
    }

    private void criarIssue(View view) {
        EditText et_nome = (EditText) view.findViewById(R.id.et_feedback_nome);
        EditText et_descricao = (EditText) view.findViewById(R.id.et_feedback_descricao);

        RadioGroup group = (RadioGroup) view.findViewById(R.id.rg_feedback_radios);

        int radioSelecionado = group.getCheckedRadioButtonId();

        BitbucketAPI.Issue issue = null;

        String tipo = "";

        if (radioSelecionado == R.id.rb_feedback_bug) {
            tipo = "bug";
        } else if (radioSelecionado == R.id.rb_feedback_sugestao) {
            tipo = "enhancement";
        }

        StringBuilder descricao = new StringBuilder();

        descricao.append("**Nome: **").append(et_nome.getText().toString());
        descricao.append("\n\n**Descrição: **").append(et_descricao.getText().toString());
        descricao.append("\n\n**Activity: **").append(EncontrarPostosActivity.class);
        descricao.append("\n\n**Versão: **").append(Build.VERSION.RELEASE);
        descricao.append("\n\n**Versão SDK: ** API ").append(Build.VERSION.SDK_INT);
        descricao.append("\n\n**Dispositivo: **").append(Build.DEVICE);
        descricao.append("\n\n**Modelo: **").append(Build.MODEL);
        descricao.append("\n\n**Localização: **").append(context.getResources().getConfiguration().locale.getDisplayCountry());

        Log.i("DESCRIÇÃO", descricao.toString());

        issue = new BitbucketAPI.Issue(et_descricao.getText().toString(), descricao.toString(), tipo);

        BitbucketAPI.criarIssue(issue);
    }
}
